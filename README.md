# MaterialStepperView

#### 项目介绍
- 项目名称：MaterialStepperView
- 所属系列：openharmony的第三方组件适配移植
- 功能：竖直样式的 Stepper 组件,未来将会加入更多的样式。你可以自定义正常/激活的圆点颜色、完成图标、动画时长、是否启用动画、线条颜色以及错误高亮颜色之类的参数。
- 项目移植状态：主功能已完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release v0.2.5

#### 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/MaterialStepperView/raw/master/img/record.gif "record.gif")

#### 安装教程

1、在项目根目录下的build.gradle文件中
```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2.在entry模块的build.gradle文件中
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:MaterialStepperView:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
- 目前我们只做了竖直样式的 Stepper 组件。未来将会加入更多的样式。
你可以自定义正常/激活的圆点颜色、完成图标、动画时长、是否启用动画、线条颜色以及错误高亮颜色之类的参数。
```示例XML
 <moe.feng.common.stepperview.VerticalStepperItemView
            ohos:id="$+id:stepper_0"
            ohos:height="match_content"
            ohos:top_margin="24vp"
            ohos:width="match_parent"
            app:step_index="1"
            app:step_state="selected"
            app:step_summary="Summarized if needed"
            app:step_summary_done="Summarized if needed. Done"
            app:step_title="Step 1" >
</moe.feng.common.stepperview.VerticalStepperItemView>


<moe.feng.common.stepperview.VerticalStepperView
		ohos:id="$+id:vertical_stepper_view"
		ohos:width="match_parent"
		ohos:height="match_parent"
		app:step_done_icon="$graphic:ic_done_white_16dp"
		app:step_animation_duration="300"/>
```
- 属性介绍
  step_index  当前第几步
  step_state  当前的状态
  step_summary 当前状态信息
  step_summary_done  当前的状态
  step_title   当前标题
  step_done_icon  完成图标
  step_animation_duration  执行动画时间


#### 测试信息
- CodeCheck代码测试无异常
- CloudTest代码测试无异常
- 病毒安全检测通过
- 当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0

#### 版权和许可信息
```
MIT License

Copyright (c) 2017-2020 Siubeng Fung (fython)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

```
