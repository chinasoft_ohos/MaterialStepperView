## 1.0.0
* 正式版发布

## 0.0.2-SNAPSHOT

ohos 第二个版本
* 修改REDME.md
* 新增README.OPENSOURCE

## 0.0.1-SNAPSHOT

ohos 第一个版本
* 实现了原库全部主功能
* 因为手势拖拽未实现原因，Demo中侧滑未实现拖拽功能
