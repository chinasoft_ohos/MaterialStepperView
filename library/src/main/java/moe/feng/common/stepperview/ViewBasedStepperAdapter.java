package moe.feng.common.stepperview;

import ohos.agp.components.Component;
import ohos.app.Context;

public abstract class ViewBasedStepperAdapter implements IStepperAdapter {
    private Component mViews[];
    public ViewBasedStepperAdapter() {
    }

//    public ViewBasedStepperAdapter(Component[] views) {
//        mViews = views;
//    }
//
//    public void setViews(Component[] views) {
//        mViews = views;
//    }

    public Component getView(int index) {
        return mViews[index];
    }

    @Override
    public int size() {
        return mViews.length;
    }

    @Override
    public Component onCreateCustomView(int index, Context context, VerticalStepperItemView parent) {
        return mViews[index];
    }
}
