/*
 * Copyright (C) 2021 The Chinese Software International Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package moe.feng.common.stepperview;

import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;

/**
 * ElementColorUtil工具列
 *
 * @since 2021-04-13
 */
public class ElementColorUtil {
    private static final int SIZE = 2;
    private static final int COUNT = -1;

    /**
     * 修改Element颜色
     *
     * @param element
     * @param intColor
     * @return Element
     */
    public static Element getTintElement(Element element, int intColor) {
        int[] colors = new int[]{intColor, intColor};
        int[][] states = new int[SIZE][];
        states[0] = new int[]{1};
        states[1] = new int[]{COUNT};
        element.setStateColorList(states, colors);
        element.setStateColorMode(BlendMode.SRC_ATOP);
        return element;
    }

    public static Element getTintElement(Element element, String colorStr) {
        int intColor = Color.getIntColor(colorStr);
        return getTintElement(element, intColor);
    }

    public static Element getTintElement(Element element, int intColor, BlendMode mode) {
        int[] colors = new int[]{intColor, intColor};
        int[][] states = new int[2][];
        states[0] = new int[]{0};
        states[1] = new int[]{0};
        element.setStateColorList(states, colors);
        element.setStateColorMode(mode);
        return element;
    }

    public static void setImageTint(Image view, Element element, String colorStr) {
        getTintElement(element, colorStr);
        setImageElement(view, element);
    }

    public static void setImageTint(Image view, Element element, int intColor) {
        getTintElement(element, intColor);
        setImageElement(view, element);
    }

    public static void setImageTint(Image view, Element element, int intColor, BlendMode mode) {
        getTintElement(element, intColor, mode);
        setImageElement(view, element);
    }

    public static void setImageElement(Image view, Element element) {
        view.setImageElement(element);
    }
}
