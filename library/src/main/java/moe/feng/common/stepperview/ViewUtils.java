/*
 * Copyright (C) 2021 The Chinese Software International Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package moe.feng.common.stepperview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

/**
 * ViewUtils
 *
 * @since 2021-04-13
 */
public class ViewUtils {
    /**
     * getRgbColor
     *
     * @param color color
     * @return RgbColor
     */
    public static RgbColor getRgbColor(Color color) {
        return RgbColor.fromArgbInt(color.getValue());
    }

    /**
     * getRgbColor
     *
     * @param color color
     * @return RgbColor
     */
    public static RgbColor getRgbColor(int color) {
        return RgbColor.fromArgbInt(color);
    }

    public static AnimatorValue createArgbAnimator(Component view, String propertyName, int startColor, int endColor) {
        AnimatorValue animation = new AnimatorValue();
        animation.setDuration(200);
        animation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        animation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float interpolatedTime) {
                ShapeElement shapeElement = new ShapeElement();
                if (interpolatedTime < 0.5) {
                    shapeElement.setRgbColor(ViewUtils.getRgbColor(startColor));
                    if (view instanceof Text) {
                        Text text = (Text) view;
                        text.setTextColor(new Color(startColor));
                    }
                } else {
                    if (view instanceof Text) {
                        Text text = (Text) view;
                        text.setTextColor(new Color(endColor));
                    }
                    shapeElement.setRgbColor(ViewUtils.getRgbColor(endColor));
                }
                if (view instanceof Text) {
                } else {
                    shapeElement.setShape(ShapeElement.OVAL);
                    view.setBackground(shapeElement);
                }
            }
        });
        return animation;
    }

    public static AnimatorValue createArgbAnimatorColor(Component view, String propertyName, Color startColor, int endColor) {

        AnimatorValue animation = new AnimatorValue();
        animation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        animation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {

            @Override
            public void onUpdate(AnimatorValue animatorValue, float interpolatedTime) {
                ShapeElement shapeElement = new ShapeElement();
                if (interpolatedTime < 0.5) {
                    shapeElement.setRgbColor(ViewUtils.getRgbColor(startColor));
                    if (view instanceof Text) {
                        Text text = (Text) view;
                        text.setTextColor(startColor);
                    }
                } else {
                    if (view instanceof Text) {
                        Text text = (Text) view;
                        text.setTextColor(new Color(endColor));
                    }
                    shapeElement.setRgbColor(ViewUtils.getRgbColor(endColor));
                }
                if (view instanceof Text) {
                } else {
                    view.setBackground(shapeElement);
                }
            }
        });
        return animation;
    }

    public static AnimatorValue createShowAnimator(Component showview, int startColor,String propertyName, Component hideview) {
        AnimatorValue animation = new AnimatorValue();
        animation.setDuration(200);
        animation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        animation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {

            @Override
            public void onUpdate(AnimatorValue animatorValue, float interpolatedTime) {
                ShapeElement shapeElement = new ShapeElement();
                System.out.println("zzzzm-->" + "getComponent2eeee---" + interpolatedTime);

                if (interpolatedTime < 0.5) {
                    shapeElement.setRgbColor(ViewUtils.getRgbColor(startColor));
                    shapeElement.setShape(ShapeElement.OVAL);
                    hideview.setBackground(shapeElement);
                } else {
                    showview.setVisibility(Component.VISIBLE);
                    hideview.setVisibility(Component.HIDE);

                }
            }
        });
        return animation;
    }

}
