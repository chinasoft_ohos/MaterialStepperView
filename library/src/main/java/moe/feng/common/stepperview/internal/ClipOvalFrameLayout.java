package moe.feng.common.stepperview.internal;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Path;
import ohos.app.Context;
/**
 * ClipOvalFrameLayout
 *
 * @since 2021-04-13
 */
public class ClipOvalFrameLayout extends StackLayout implements Component.DrawTask, Component.EstimateSizeListener {
    private Path path = new Path();

    public ClipOvalFrameLayout(Context context) {
        super(context);
        init();
    }

    public ClipOvalFrameLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public ClipOvalFrameLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
//        ShapeElement shapeElement = new ShapeElement();
//        shapeElement.setCornerRadius(Float.valueOf(getWidth() / 2));
//        setBackground(shapeElement);
    }

    @Override
    public boolean onEstimateSize(int w, int h) {
        float halfWidth = w / 2f;
        float halfHeight = h / 2f;
        path.reset();
        path.addCircle(halfWidth, halfHeight, Math.min(halfWidth, halfHeight), Path.Direction.CLOCK_WISE);
        path.close();
        return false;
    }
}
