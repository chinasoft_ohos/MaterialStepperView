package moe.feng.common.stepperview.demo.slice;
import moe.feng.common.stepperview.IStepperAdapter;
import moe.feng.common.stepperview.VerticalStepperItemView;
import moe.feng.common.stepperview.VerticalStepperView;
import moe.feng.common.stepperview.demo.MaterialRippleLayout;
import moe.feng.common.stepperview.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;
/*
 * Copyright (C) 2021 The Chinese Software International Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
public class MainAbilitySlice extends AbilitySlice implements IStepperAdapter {
    private VerticalStepperView mVerticalStepperView;
    private DirectionalLayout itemlayout;
    private DirectionalLayout adapterlayout;
    private Image clickleft;
    private DirectionalLayout leftlayout;
    private DirectionalLayout itemview;
    private DirectionalLayout adpaterview;
    private DirectionalLayout background;
    private int click;
    private Text item;
    private Text adapt;
    private boolean isShowd = false;
    private Image blueItem;
    private Image grayAdapter;
    private Image blueAdapter;
    private Image grayItem;
    private DirectionalLayout donnate;
    private DirectionalLayout foke;
    private VerticalStepperItemView mSteppers[] = new VerticalStepperItemView[3];
    private Button mNextBtn0;
    private Button mNextBtn1;
    private Button mPrevBtn1;
    private Button mNextBtn2;
    private Button mPrevBtn2;
    private String mActivatedColorRes = "#2196F3";
    private int mDoneIconRes = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initLeft();
        initCommont();
        mVerticalStepperView = (VerticalStepperView) findComponentById(ResourceTable.Id_vertical_stepper_view);
        mVerticalStepperView.setStepperAdapter(this,MainAbilitySlice.this);
    }
    private void showDialog() {
        Component dialogcommont = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dialog, null, false);
        Text pay = (Text) dialogcommont.findComponentById(ResourceTable.Id_pay);
        Text ok = (Text) dialogcommont.findComponentById(ResourceTable.Id_ok);
        CommonDialog commonDialog = new CommonDialog(this);
        commonDialog.setContentCustomComponent(dialogcommont);
        commonDialog.setSize(1000, 520);
        commonDialog.show();
        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                commonDialog.hide();
            }
        });
        pay.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intents = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withAction(IntentConstants.ACTION_SEARCH)
                        .withUri(Uri.parse("https://paypal.me/fython"))
                        .build();
                intents.setOperation(operation);
                startAbility(intents);
                commonDialog.hide();
            }
        });
    }

    private void initLeft() {
        itemlayout = (DirectionalLayout) findComponentById(ResourceTable.Id_item_layout);
        adapterlayout = (DirectionalLayout) findComponentById(ResourceTable.Id_adapter_item);
        donnate = (DirectionalLayout) findComponentById(ResourceTable.Id_donate);
        grayItem = (Image) findComponentById(ResourceTable.Id_gray_item);
        blueItem = (Image) findComponentById(ResourceTable.Id_blue_item);
        grayAdapter = (Image) findComponentById(ResourceTable.Id_gray_adapter);
        blueAdapter = (Image) findComponentById(ResourceTable.Id_blue_adapter);
        foke = (DirectionalLayout) findComponentById(ResourceTable.Id_foke);
        blueItem.setVisibility(Component.VISIBLE);
        grayItem.setVisibility(Component.HIDE);
        clickleft = (Image) findComponentById(ResourceTable.Id_click_left);
        leftlayout = (DirectionalLayout) findComponentById(ResourceTable.Id_left_menu);
        background = (DirectionalLayout) findComponentById(ResourceTable.Id_background);
        itemview = (DirectionalLayout) findComponentById(ResourceTable.Id_itmview);
        adpaterview = (DirectionalLayout) findComponentById(ResourceTable.Id_adpaterview);
        item = (Text) findComponentById(ResourceTable.Id_item);
        adapt = (Text) findComponentById(ResourceTable.Id_adapt);
        background.setVisibility(Component.HIDE);
        background.setAlpha(0.4f);
        initleftmenu();
    }

    private void initleftmenu() {
        ShapeElement backgrounds = new ShapeElement();
        backgrounds.setRgbColor(new RgbColor(158, 158, 158));
        itemview.setBackground(backgrounds);
        item.setTextColor(new Color(Color.getIntColor("#2196F3")));
        clickleft.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!isShowd) {
                    if (click == 0) {
                        ShapeElement backgrounds = new ShapeElement();
                        backgrounds.setRgbColor(new RgbColor(158, 158, 158));
                        itemview.setBackground(backgrounds);
                        item.setTextColor(new Color(Color.getIntColor("#2196F3")));
                        blueItem.setVisibility(Component.VISIBLE);
                        grayItem.setVisibility(Component.HIDE);
                        grayAdapter.setVisibility(Component.VISIBLE);
                        blueAdapter.setVisibility(Component.HIDE);
                        adapt.setTextColor(Color.BLACK);
                        ShapeElement background = new ShapeElement();
                        background.setRgbColor(new RgbColor(255, 255, 255));
                        adpaterview.setBackground(background);
                    } else {
                        ShapeElement backgrounds = new ShapeElement();
                        backgrounds.setRgbColor(new RgbColor(158, 158, 158));
                        adpaterview.setBackground(backgrounds);
                        ShapeElement background = new ShapeElement();
                        background.setRgbColor(new RgbColor(255, 255, 255));
                        itemview.setBackground(background);
                        adapt.setTextColor(new Color(Color.getIntColor("#2196F3")));
                        item.setTextColor(Color.BLACK);

                        blueItem.setVisibility(Component.HIDE);
                        grayItem.setVisibility(Component.VISIBLE);
                        grayAdapter.setVisibility(Component.HIDE);
                        blueAdapter.setVisibility(Component.VISIBLE);
                    }
                    animal(false);
                }
            }
        });
        donnate.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                animal(true);
                showDialog();
            }
        });
        background.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (isShowd) {
                    animal(true);
                }
            }
        });
        itemview.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                click = 0;
                ShapeElement backgrounds = new ShapeElement();
                backgrounds.setRgbColor(new RgbColor(158, 158, 158));
                itemview.setBackground(backgrounds);
                ShapeElement background = new ShapeElement();
                background.setRgbColor(new RgbColor(255, 255, 255));
                adpaterview.setBackground(background);
                blueItem.setVisibility(Component.VISIBLE);
                grayItem.setVisibility(Component.HIDE);
                grayAdapter.setVisibility(Component.VISIBLE);
                blueAdapter.setVisibility(Component.HIDE);
                itemlayout.setVisibility(Component.VISIBLE);
                adapterlayout.setVisibility(Component.HIDE);
                item.setTextColor(new Color(Color.getIntColor("#2196F3")));
                adapt.setTextColor(Color.BLACK);
                if (isShowd) {
                    animal(true);
                }
            }
        });
        adpaterview.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                click = 1;
                ShapeElement backgrounds = new ShapeElement();
                backgrounds.setRgbColor(new RgbColor(158, 158, 158));
                adpaterview.setBackground(backgrounds);
                adapt.setTextColor(new Color(Color.getIntColor("#2196F3")));
                item.setTextColor(Color.BLACK);
                ShapeElement background = new ShapeElement();
                background.setRgbColor(new RgbColor(255, 255, 255));
                itemview.setBackground(background);
                itemlayout.setVisibility(Component.HIDE);
                adapterlayout.setVisibility(Component.VISIBLE);
                blueItem.setVisibility(Component.HIDE);
                grayItem.setVisibility(Component.VISIBLE);
                grayAdapter.setVisibility(Component.HIDE);
                blueAdapter.setVisibility(Component.VISIBLE);
                if (isShowd) {
                    animal(true);
                }
            }
        });
        foke.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                animal(true);
                Intent intents = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withAction(IntentConstants.ACTION_SEARCH)
                        .withUri(Uri.parse("https://github.com/fython/MaterialStepperView"))
                        .build();
                intents.setOperation(operation);
                startAbility(intents);
            }
        });
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    private void initCommont() {
        mSteppers[0] = (VerticalStepperItemView) findComponentById(ResourceTable.Id_stepper_0);
        mSteppers[1] = (VerticalStepperItemView) findComponentById(ResourceTable.Id_stepper_1);
        mSteppers[2] = (VerticalStepperItemView) findComponentById(ResourceTable.Id_stepper_2);
        Component inflateView0 = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_step_0_layout, null, false);
        Component inflateView1 = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_step_1_layout, null, false);
        Component inflateView2 = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_step_2_layout, null, false);
        if(null != mSteppers[0] && null != mSteppers[1] && null != mSteppers[2] && null != inflateView0 && null != inflateView1 && null != inflateView2) {
            mSteppers[0].addCustomViewComponet(inflateView0);
            mSteppers[1].addCustomViewComponet(inflateView1);
            mSteppers[2].addCustomViewComponet(inflateView2);
            VerticalStepperItemView.bindSteppers(mSteppers);
            mNextBtn0 = (Button) inflateView0.findComponentById(ResourceTable.Id_button_next_0);
            startAnimator(this, mNextBtn0, "#007DFF");
            mNextBtn0.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    mSteppers[0].nextStep();
                }
            });

            Button mErrorButton = (Button) inflateView1.findComponentById(ResourceTable.Id_button_test_error);
            startAnimator(this, mErrorButton, "#F44336");
            mErrorButton.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    if (mSteppers[0].getErrorText() != null) {
                        mSteppers[0].setErrorText(0, null);
                    } else {
                        mSteppers[0].setErrorText("Test error!");
                    }
                }
            });

            Button mPrevBtn0 = (Button) inflateView0.findComponentById(ResourceTable.Id_button_pre_0);
            startAnimator(this, mPrevBtn0, "#ECECEC");
            mPrevBtn0.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                }
            });

            mPrevBtn1 = (Button) inflateView1.findComponentById(ResourceTable.Id_button_prev_1);
            startAnimator(this, mPrevBtn1, "#ECECEC");
            mPrevBtn1.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    mSteppers[1].prevStep();
                }
            });
            mNextBtn1 = (Button) inflateView1.findComponentById(ResourceTable.Id_button_next_1);
            startAnimator(this, mNextBtn1, "#007DFF");
            mNextBtn1.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    mSteppers[1].nextStep();
                }
            });
            mPrevBtn2 = (Button) inflateView2.findComponentById(ResourceTable.Id_button_prev_2);
            startAnimator(this, mPrevBtn2, "#ECECEC");
            mPrevBtn2.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    mSteppers[2].prevStep();
                }
            });
            mNextBtn2 = (Button) inflateView2.findComponentById(ResourceTable.Id_button_next_2);
            startAnimator(this, mNextBtn2, "#007DFF");
            mNextBtn2.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    toast(MainAbilitySlice.this, "Finish!");
                }
            });
            Button chahge = (Button) findComponentById(ResourceTable.Id_btn_change_point_color);
            startAnimator(this, chahge, "#ECECEC");
            chahge.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    if (mActivatedColorRes.equals("#2196F3")) {
                        mActivatedColorRes = "#673AB7";
                    } else {
                        mActivatedColorRes = "#2196F3";
                    }
                    for (VerticalStepperItemView stepper : mSteppers) {
                        stepper.setActivatedColor(Color.getIntColor(mActivatedColorRes));
                    }
                }
            });
            Button chahge1 = (Button) findComponentById(ResourceTable.Id_btn_change_done_icon);
            startAnimator(this, chahge1, "#ECECEC");
            chahge1.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    if (mDoneIconRes == ResourceTable.Graphic_ic_done_white_16dp) {
                        mDoneIconRes = ResourceTable.Graphic_ic_save_white_16dp;
                    } else {
                        if (mDoneIconRes == 0) {
                            mDoneIconRes = ResourceTable.Graphic_ic_save_white_16dp;
                        } else {
                            mDoneIconRes = ResourceTable.Graphic_ic_done_white_16dp;
                        }
                    }
                    for (VerticalStepperItemView stepper : mSteppers) {
                        stepper.setDoneIconResource(mDoneIconRes);
                    }
                }
            });
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public String getTitle(int index) {
        return "Step " + index;
    }

    public String getSummary(int index) {
        switch (index) {
            case 0:
                return "Summarized if needed" + (mVerticalStepperView.getCurrentStep() > index ? ";isDone!" : "");
            case 2:
                return "Last step"
                        + (mVerticalStepperView.getCurrentStep() > index ? "; isDone!" : "");
            default:
                return "";
        }
    }

    @Override
    public int size() {
        return 3;
    }

    @Override
    public Component onCreateCustomView(final int index, Context context, VerticalStepperItemView parent) {
        Component inflateView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_vertical_stepper_sample_item, null, false);
        Text contentView = (Text) inflateView.findComponentById(ResourceTable.Id_item_content);
        contentView.setText(
                index == 0 ? ResourceTable.String_content_step_0 : (index == 1 ? ResourceTable.String_content_step_1 : ResourceTable.String_content_step_2)
        );
        Button nextButton = (Button) inflateView.findComponentById(ResourceTable.Id_button_next);
        nextButton.setText(index == size() - 1 ? "SET ERROR TEXT" : "OK");
        nextButton.setWidth(nextButton.getText().toString().equals("OK") ? 220 : 480);
        startAnimator(this, nextButton, "#007DFF");
        nextButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!mVerticalStepperView.nextStep()) {
                    mVerticalStepperView.setErrorText(0, mVerticalStepperView.getErrorText(0) == null ? "Test error" : null);
                    toast(MainAbilitySlice.this, "Set!");
                }
            }
        });
        Button prevButton = (Button) inflateView.findComponentById(ResourceTable.Id_button_prev);
        prevButton.setText(index == 0 ? "TOGGLE ANIMATION" : "CANCLE");
        prevButton.setWidth(prevButton.getText().toString().equals("CANCLE") ? 220 : 480);
        startAnimator(this, prevButton, "#ECECEC");
        prevButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (index != 0) {
                    mVerticalStepperView.prevStep();
                } else {
                    mVerticalStepperView.setAnimationEnabled(!mVerticalStepperView.isAnimationEnabled());
                }
            }
        });
        return inflateView;
    }

    @Override
    public void onShow(int index) {
    }

    @Override
    public void onHide(int index) {
    }

    private void animal(boolean isShow) {
        AnimatorValue animation = new AnimatorValue();
        animation.setDuration(100);
        animation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                if (!isShow) {
                    background.setVisibility(Component.VISIBLE);
                } else {
                    background.setVisibility(Component.HIDE);
                }
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        animation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float interpolatedTime) {
                double newHeight = 0;
                if (!isShow) {
                    newHeight =  880 * (double)interpolatedTime;
                    isShowd = true;
                } else {
                    newHeight = 880 * (1 - (double)interpolatedTime);
                    isShowd = false;
                }
                leftlayout.setWidth((int)(newHeight));
            }
        });
        animation.start();
    }

    private static final int MAX_LINE = 2;
    private static final int OFFSET_Y = 60;

    private void toast(Context context, String string) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_toast, null, false);
        Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText(string);
        text.setTextAlignment(TextAlignment.LEFT);
        text.setMultipleLine(true);
        text.setMaxTextLines(MAX_LINE);
        text.setTruncationMode(Text.TruncationMode.ELLIPSIS_AT_END);
        new ToastDialog(context)
                .setComponent(toastLayout)
                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.BOTTOM | LayoutAlignment.HORIZONTAL_CENTER)
                .setOffset(0, OFFSET_Y)
                .show();
    }

    private static final float RIPPLE_ALPHA = 0.2f;
    private static final int RIPPLE_DURATION = 2000;
    private static final int RIPPLE_DIAMETERDP = 35;
    private static MaterialRippleLayout materialRippleLayout;

    public static void startAnimator(Context context, Button bt, String color) {
        if (bt != null) {
            try {
                materialRippleLayout = new MaterialRippleLayout(context).on(bt)
                        .rippleColor(Color.getIntColor("#86666666"))
                        .rippleAlpha(RIPPLE_ALPHA)
                        .rippleDuration(RIPPLE_DURATION)
                        .rippleBackground(Color.getIntColor(color))
                        .rippleOverlay(true)
                        .rippleDiameterDp(RIPPLE_DIAMETERDP)
                        .create();
            } catch (Exception e) {
                e.fillInStackTrace();
            }

        }
    }
}
